# A tool to build the operating system/package images that are then dynamically mounted.

This tool is used to build operating system/package images for MatuushOS. The images are in SquashFS format, making them
easily manageable and universal.

## Configuration example

```ron
(
    name: "",
    description: "",
    type_of_payload: OSImage,
    version: 0.0,
    source: "",
    building: (
        prepare: [],
        build: [],
        install: [],
    ),
)
```

# What all those image types mean?

![](img/image.png "Diagram")

# What should we do next?

- [] Automatically upload images to CDN for faster downloads
- [] Integrate this tool with PM
- [x] have prettier output with the [`ansi-term`](https://lib.rs/crates/ansi_term) crate