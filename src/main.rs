#![warn(rust_2024_compatibility,
	clippy::all,
	clippy::pedantic,
	clippy::cargo,
	clippy::correctness
)]
#![doc = include_str!("../README.md")]

mod utils;

fn main() -> anyhow::Result<(), anyhow::Error> {
	utils::Builder::construct()?;
	Ok(())
}
