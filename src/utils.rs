use std::borrow::Borrow;
use std::path::Path;
use std::string::ToString;

use clap::Parser;
use git2::Repository;
use ron::ser::PrettyConfig;
use serde::{Deserialize, Serialize};

use crate::utils::OSImageKind::Update;
use crate::utils::TypeOfPayload::{OSImage, Package};

#[derive(Serialize, Deserialize, Debug)]
#[non_exhaustive]
enum OSImageKind {
	#[non_exhaustive]
	Update { version: Vec<i32>, is_major: bool },
	#[non_exhaustive]
	Bootstrap {
		minimal: bool,
		version: Vec<i32>,
		is_major: bool,
	},
}

#[derive(Serialize, Deserialize, Debug)]
#[non_exhaustive]
struct OSPayloadInfo {
	release: Vec<i32>,
	kind: OSImageKind,
	contains_core_pkg_update: bool,
}

/// Type of packages
#[derive(Serialize, Deserialize, Debug)]
#[non_exhaustive]
enum TypeOfPayload {
	///Operating system image
	OSImage {
		/// Gets the kind of the image
		///
		kind: OSImageKind,
	},
	///Package image
	Package {
		/// Checks if the application is graphical. If set to `true`, Imager will symlink the
		/// `.desktop` file to ~/.local/share/applications/PM
		is_graphical: bool,
	},
}

#[derive(Parser)]
#[clap(
	name = "Imager",
	about = "A system for building package/OS update images for MatuushOS",
	long_about = "System that builds images for MatuushOS.\nThe images can be:\n\t- OS update image (though they can be used as bootstrap images)\
    \n\t- Package image\r\n
    These images can be uploaded to package registry (which is then set in `pm`) to allow easy downloads and are sandboxed by Firejail."
)]
/// CLI args for Clap
#[non_exhaustive]
pub(crate) struct Cli {
	/// Build the image
	#[arg(short, long)]
	pub(crate) build: String,
}

/// Properties for the metadata file
#[derive(Serialize, Deserialize)]
#[non_exhaustive]
struct MetaFile {
	name: String,
	desc: String,
	type_of_payload: TypeOfPayload,
	source: String,
}

#[derive(Serialize, Deserialize, Default)]
pub(crate) struct Building {
	prepare: Vec<String>,
	build: Vec<String>,
	install: Vec<String>,
}

#[derive(Serialize, Deserialize)]
#[non_exhaustive]
pub(crate) struct Builder {
	name: String,
	///Description
	description: String,
	///Type of payload
	type_of_payload: TypeOfPayload,
	///Version
	version: Vec<i32>,
	/// Source.
	/// Must be a Git repository
	sources: String,
	/// Build steps.
	/// Build system performs automatic word splitting to distinct the build command from
	/// arguments
	building: Building,
	/// Local dependencies.
	/// Required if used for building an image
	localdeps: Vec<String>,
}

impl Builder {
	/// Initializes imager configuration file
	///# How to implement `Vec<String>` into the Command:
	///1. Take the first element of the array from Vec
	///2. `let cmd = i.iter();`
	///3. `Command::new(cmd[0]).args(vec![cmd.next()]);`
	
	/// This is used for building the image
	/// # Errors
	/// Will fail if the function can't download the sources, or if prepare/build/install phase fails.
	pub(crate) fn construct() -> anyhow::Result<(), anyhow::Error> {
		let f = std::fs::read_to_string(Cli::parse().build)?;
		let cfg: Self = ron::from_str::<Self>(&f)?;
		let config = cfg.borrow();
		match config.type_of_payload {
			OSImage {
				kind: Update { .. },
			} => Self::build_osimage(
				f,
				&Update {
					version: vec![],
					is_major: false,
				},
			),
			OSImage {
				kind: OSImageKind::Bootstrap { .. },
			} => Self::build_osimage(
				f,
				&OSImageKind::Bootstrap {
					minimal: false,
					version: vec![],
					is_major: false,
				},
			),
			Package {
				is_graphical: true | false,
			} => Ok({
				// Write metadata file to the package directory
				let meta = MetaFile {
					name: cfg.name,
					desc: cfg.description,
					type_of_payload: cfg.type_of_payload,
					source: cfg.sources,
				};
				for i in &["build", "payload"] {
					std::fs::create_dir(i)?;
					println!(
						"\t{} Created directory {i}",
						ansi_term::Color::Green.paint(">>")
					);
				}
				println!(
					"{} Starting build of payload {}, version {:#?}, of which type of payload is {:?}",
					ansi_term::Color::Green.paint(">>"),
					ansi_term::Color::Blue.underline().paint(meta.name),
					ansi_term::Color::Blue.underline().paint(cfg.version),
					meta.type_of_payload
				);
				println!("{} Downloading the source", ansi_term::Color::Green.paint(">>"));
				Repository::clone(&meta.source, Path::new(".").join("build"))?;
				std::process::Command::new(
					cfg.building
					   .prepare
					   .clone()
					   .into_iter()
					   .nth(0)
					   .is_some()
					   .to_string(),
				)
					.args(&cfg.building.prepare.into_iter().next())
					.current_dir("build")
					.output()?;
				
				std::process::Command::new(
					cfg.building
					   .build
					   .clone()
					   .into_iter()
					   .nth(0)
					   .is_some()
					   .to_string(),
				)
					.args(&cfg.building.build.into_iter().next())
					.current_dir("build")
					.output()?;
				
				std::process::Command::new(
					cfg.building.install.clone().first().is_some().to_string(),
				)
					.args(&cfg.building.install.into_iter().next())
					.current_dir("build")
					.env("INSTROOT", Path::new("../payload"))
					.output()?;
				println!(">> Packaging the built binaries");
				std::process::Command::new("mksquashfs")
					.arg(Path::new("../build"))
					.output()?;
			}),
		}?;
		Ok(())
	}
	#[doc(hidden)]
	pub(crate) fn construct_localdeps(f: Vec<String>) -> anyhow::Result<(), anyhow::Error> {
		for t in f {
			let cfg: Builder = ron::from_str(&std::fs::read_to_string(t)?)?;
			let meta = MetaFile {
				name: cfg.name,
				desc: cfg.description,
				type_of_payload: cfg.type_of_payload,
				source: cfg.sources,
			};
			for i in &["build", "payload"] {
				std::fs::create_dir(Path::new(&meta.name).join(i))?;
				println!("\t{} Created directory {i}", ansi_term::Color::Green.paint(">>"));
			}
			println!(
				"{} Starting build of payload {}, version {:#?}, of which type of payload is {:#?}",
				ansi_term::Color::Green.paint(">>"),
				meta.name, cfg.version, meta.type_of_payload
			);
			println!("{} Downloading the source(s)", ansi_term::Color::Green.paint(">>"));
			Repository::clone(&meta.source, Path::new(".").join("build"))?;
			std::process::Command::new(
				cfg.building
				   .prepare
				   .clone()
				   .into_iter()
				   .nth(0)
				   .is_some()
				   .to_string(),
			)
				.args(&cfg.building.prepare.into_iter().next())
				.current_dir("build")
				.output()?;
			
			std::process::Command::new(
				cfg.building
				   .build
				   .clone()
				   .into_iter()
				   .nth(0)
				   .is_some()
				   .to_string(),
			)
				.args(&cfg.building.build.into_iter().next())
				.current_dir("build")
				.output()?;
			
			std::process::Command::new(cfg.building.install.clone().first().is_some().to_string())
				.args(&cfg.building.install.into_iter().next())
				.current_dir("build")
				.env("INSTROOT", Path::new("../payload"))
				.output()?;
			println!(">> Packaging the built binaries");
			std::process::Command::new("mksquashfs")
				.arg(Path::new("../build"))
				.output()?;
		}
		Ok(())
		// Write metadata file to the package directory
	}
	#[doc(hidden)]
	fn build_osimage(file: String, kind: &OSImageKind) -> anyhow::Result<(), anyhow::Error> {
		let cfg: Builder = ron::from_str::<Builder>(&std::fs::read_to_string(file)?)?;
		let arrow = ansi_term::Color::Green.paint(">>");
		let exmark = ansi_term::Color::Red.paint("!!");
		match kind {
			Update { .. } => {
				println!("{} Making an update image\n{} Creating directories", arrow, arrow);
				for i in &["build/", "build/mounted", "build/etc", "build/firejail"] {
					std::fs::create_dir_all(i)?;
					println!("\t{} Created directory {i}", arrow);
				}
				let meta: OSPayloadInfo = OSPayloadInfo {
					release: (*cfg.version).to_owned(),
					kind: Update {
						version: cfg.version,
						is_major: false,
					},
					contains_core_pkg_update: false,
				};
				match std::fs::write(
					"build/updinfo.ron",
					ron::ser::to_string_pretty(&meta, PrettyConfig::default())?,
				) {
					Ok(f) => println!("{} Metadata file {f:#?} sucessfully written", arrow),
					Err(e) => {
						eprintln!("{} An error happened.\n!! Reason: {e:#?}", exmark);
					}
				}
				std::fs::create_dir("packages")?;
				for i in cfg.localdeps {
					Self::construct_localdeps(vec![i])?;
				}
				// Assuming that build scripts installed the sources in root of the payload, we won't have to compress the packages folder
				println!(">> Built the update payload\n>> Compressing the payload");
				std::process::Command::new("mksquashfs")
					.arg("build")
					.output()?;
				println!(">> DONE");
			}
			OSImageKind::Bootstrap { .. } => unimplemented!("Not yet implemented, unfortunately"),
		};
		
		Ok(())
	}
}
